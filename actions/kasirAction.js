export const GET_LIST_MENU = "GET_LIST_MENU";
export const GET_LIST_CART = "GET_LIST_CART";
export const ADD_TO_CART = "ADD_TO_CART";
export const DELETE_CART = "DELETE_CART";
export const MINUS_QTY = "MINUS_QTY";
export const PLUS_QTY = "PLUS_QTY";
export const TAX = "TAX";
export const RESET_CART = "RESET_CART";
export const SET_MENU = "SET_MENU";
export const CATEGORY_DROPDOWN_MENU = "CATEGORY_DROPDOWN_MENU";

export const getListMenu = () => {
  console.log("2. masuk action gara2 getlistmenu()");
  return (dispatch) => {
    //loading
    console.log("3. Loading..");
    dispatch({
      type: GET_LIST_MENU,
      payload: {
        loading: true,
        data: false,
        errorMessage: false,
      },
    });

    //get data bisa dari api

    const produk = [
      {
        id: 1,
        kategori: "hot",
        name: "Kwetiau",
        price: 12000,
        image: "/img/bakso.jpeg",
        barcode: 1111111111111,
      },
      {
        id: 2,
        kategori: "makanan",
        name: "Bakso",
        price: 15000,
        image: "/img/bakso.jpeg",
        barcode: 222222222222,
      },
      {
        id: 3,
        kategori: "minuman",
        name: "Martabak",
        price: 10000,
        image: "/img/bakso.jpeg",
        barcode: 333333333,
      },
      {
        id: 4,
        kategori: "minuman",
        name: "Cileok",
        price: 10000,
        image: "/img/bakso.jpeg",
        barcode: 4444444444,
      },
      {
        id: 5,
        kategori: "minuman",
        name: "Cilok",
        price: 10000,
        image: "/img/bakso.jpeg",
        barcode: 5555555,
      },
      {
        id: 6,
        kategori: "minuman",
        name: "Cilok",
        price: 10000,
        image: "/img/bakso.jpeg",
        barcode: 6666666,
      },
      {
        id: 7,
        kategori: "minuman",
        name: "Cilok",
        price: 10000,
        image: "/img/bakso.jpeg",
        barcode: 7777777,
      },
      {
        id: 8,
        kategori: "minuman",
        name: "Cilok",
        price: 10000,
        image: "/img/bakso.jpeg",
        barcode: 8888888888,
      },
      {
        id: 9,
        kategori: "minuman",
        name: "Cilok",
        price: 10000,
        image: "/img/bakso.jpeg",
        barcode: 929999999999,
      },
      {
        id: 10,
        kategori: "minuman",
        name: "Cilok",
        price: 10000,
        image: "/img/bakso.jpeg",
        barcode: 237777333,
      },
    ];
    console.log("4. get data");
    // set to get list cart
    dispatch({
      type: GET_LIST_MENU,
      payload: {
        loading: false,
        data: produk,
        errorMessage: false,
      },
    });
  };
};

export const addToCart = (payload) => {
  return (dispatch) => {
    console.log(payload);
    dispatch({
      type: ADD_TO_CART,
      payload: {
        loading: false,
        data: payload,
        errorMessage: false,
      },
    });
  };
};
export const deleteCart = (payload) => {
  return (dispatch) => {
    console.log(payload);
    dispatch({
      type: DELETE_CART,
      payload,
    });
  };
};
export const minusQty = (payload) => {
  return (dispatch) => {
    console.log(payload);
    dispatch({
      type: MINUS_QTY,
      payload,
    });
  };
};
export const plusQty = (payload) => {
  return (dispatch) => {
    console.log(payload);
    dispatch({
      type: PLUS_QTY,
      payload,
    });
  };
};

export const setTax = (payload) => {
  return (dispatch) => {
    console.log(payload);
    dispatch({
      type: TAX,
      payload,
    });
  };
};

export const resetCart = () => {
  return (dispatch) => {
    dispatch({
      type: RESET_CART,
    });
  };
};

export const setMenu = (payload) => {
  return (dispatch) => {
    dispatch({
      type: SET_MENU,
      payload,
    });
  };
};

export const setCategoryDropdownMenu = (payload) => {
  return (dispatch) => {
    dispatch({
      type: CATEGORY_DROPDOWN_MENU,
      payload,
    });
  };
};

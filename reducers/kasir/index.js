import {
  GET_LIST_CART,
  GET_LIST_MENU,
  ADD_TO_CART,
  DELETE_CART,
  PLUS_QTY,
  MINUS_QTY,
  TAX,
  RESET_CART,
  SET_MENU,
  CATEGORY_DROPDOWN_MENU
} from "../../actions/kasirAction";

const initialState = {
  getListMenuResult: false,
  //init number cart dan cart
  numberCart: 0,
  tax: false,
  Carts: [],
  menu: false,
  categoryDropdownMenu: false,
};

const kasir = (state = initialState, action) => {
  switch (action.type) {
    // GET CART
    case GET_LIST_CART:
      return {
        ...state,
        Carts: state.Carts,
      };
    //   GET DAFTAR MENU PRODUK
    case GET_LIST_MENU:
      return {
        ...state,
        getListMenuResult: action.payload.data,
      };
    //   TAMBAH PRODUK KE DALAM CART
    case ADD_TO_CART:
      // Cek jika di cart belum ada datanya
      if (state.numberCart == 0) {
        let cart = {
          id: action.payload.data.id,
          qty: 1,
          name: action.payload.data.name,
          image: action.payload.data.image,
          price: action.payload.data.price,
          barcode: action.payload.data.barcode,
        };
        state.Carts.push(cart);
      } else {
        let sameProduct = false;
        state.Carts.map((product, i) => {
          if (product.id == action.payload.data.id) {
            state.Carts[i].qty++;
            sameProduct = true;
          }
        });
        if (!sameProduct) {
          let cart = {
            id: action.payload.data.id,
            qty: 1,
            name: action.payload.data.name,
            image: action.payload.data.image,
            price: action.payload.data.price,
            barcode: action.payload.data.barcode,
          };
          state.Carts.push(cart);
        }
      }
      return {
        ...state,
        numberCart: state.Carts.length,
      };
    //   HAPUS PRODUK DALAM CART
    case DELETE_CART:
      let quantity_ = state.Carts[action.payload].qty;
      return {
        ...state,
        numberCart: state.numberCart - 1,
        Carts: state.Carts.filter((item) => {
          console.log(state.Carts[action.payload].id + "wkwk");
          return item != state.Carts[action.payload];
        }),
      };
    //   JIKA QTY DI TAMBAH
    case PLUS_QTY:
      state.Carts[action.payload].qty++;
      return {
        ...state,
      };
    case MINUS_QTY:
      let qtyM = state.Carts[action.payload].qty;
      if (qtyM > 1) {
        state.Carts[action.payload].qty--;
      }
      return {
        ...state,
      };

    case TAX:
      state.tax = action.payload;
      return {
        ...state,
      };

    case RESET_CART:
      return {
        ...state,
        Carts: [],
        numberCart: 0,
        tax: false,
      };

    case SET_MENU:
      return {
        ...state,
        menu: action.payload,
      };

    case CATEGORY_DROPDOWN_MENU:
      return {
        ...state,
        categoryDropdownMenu: action.payload,
      };

    default:
      return state;
  }
};

export default kasir;

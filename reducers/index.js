import { combineReducers } from "redux";

import KasirReducer from "./kasir";

export default combineReducers({
    KasirReducer
})
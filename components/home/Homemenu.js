import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getListMenu,addToCart } from "../../actions/kasirAction";

function Centermenu() {
  const { getListMenuResult } =
    useSelector((state) => state.KasirReducer);

  // init dispatch
  const dispatch = useDispatch();
  // use effect
  useEffect(() => {
    // call get list cart
    console.log("1. use effect comp did mount");
    dispatch(getListMenu());
  }, [dispatch]);

  // set search query to empty string
  const [query, setQuery] = useState("");

  // init category
  const category = [
    {
      name: "All",
      value: "",
      icon: `<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
              <path stroke-linecap="round" stroke-linejoin="round" d="M11 3.055A9.001 9.001 0 1020.945 13H11V3.055z" />
              <path stroke-linecap="round" stroke-linejoin="round" d="M20.488 9H15V3.512A9.025 9.025 0 0120.488 9z" />
            </svg>`,
    },
    {
      name: "Hot",
      value: "hot",
      icon: `<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
              <path stroke-linecap="round" stroke-linejoin="round" d="M11 3.055A9.001 9.001 0 1020.945 13H11V3.055z" />
              <path stroke-linecap="round" stroke-linejoin="round" d="M20.488 9H15V3.512A9.025 9.025 0 0120.488 9z" />
            </svg>`,
    },
    {
      name: "Makanan",
      value: "makanan",
      icon: `<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
              <path stroke-linecap="round" stroke-linejoin="round" d="M11 3.055A9.001 9.001 0 1020.945 13H11V3.055z" />
              <path stroke-linecap="round" stroke-linejoin="round" d="M20.488 9H15V3.512A9.025 9.025 0 0120.488 9z" />
            </svg>`,
    },
    {
      name: "Minuman",
      value: "minuman",
      icon: `<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
              <path stroke-linecap="round" stroke-linejoin="round" d="M11 3.055A9.001 9.001 0 1020.945 13H11V3.055z" />
              <path stroke-linecap="round" stroke-linejoin="round" d="M20.488 9H15V3.512A9.025 9.025 0 0120.488 9z" />
            </svg>`,
    },
    {
      name: "Kerupuk",
      value: "kerupuk",
      icon: `<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
              <path stroke-linecap="round" stroke-linejoin="round" d="M11 3.055A9.001 9.001 0 1020.945 13H11V3.055z" />
              <path stroke-linecap="round" stroke-linejoin="round" d="M20.488 9H15V3.512A9.025 9.025 0 0120.488 9z" />
            </svg>`,
    },
  ];

  const handlePilihProduk = (payload) => {
    dispatch(addToCart(payload));
  };
  return (
    <>
      <div className="w-full bg-gray-100 p-4 lg:p-10">
        <h1 className="font-bold text-2xl py-3">Home</h1>

        <div>
          <div className="flex flex-col lg:flex-row justify-between py-5 lg:py-3">
            <h1 className="font-bold text-2xl">Welcome to KasirApp</h1>
          </div>
        </div>
      </div>
    </>
  );
}

export default Centermenu;

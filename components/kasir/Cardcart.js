import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { connect } from "react-redux";
import { deleteCart, minusQty, plusQty } from "../../actions/kasirAction";
import { customHelper } from "../../helpers";

function Cardcart({ deleteCart, minusQty, plusQty }) {
  const { Carts } = useSelector((state) => state.KasirReducer);

  // init dispatch
  const dispatch = useDispatch();
  // use effect
  useEffect(() => {
    // call get list cart
    console.log("1. use effect comp did mount list cart");
  }, [dispatch]);

  return (
    <>
      {/* jika dapat datanya maka di map */}
      {Carts != "" ? (
        Carts.map((value, i) => {
          return (
            <div
              key={i}
              className="rounded-lg shadow-md flex justify-between items-center p-4 mt-2"
            >
              <div>
                <img className="rounded-xl w-20" src={value.image} />
              </div>
              <div>
                <div className="flex flex-col px-2">
                  <span className="font-bold truncate">{value.name}</span>
                  <span>Rp. {customHelper.numberWithCommas(value.price)}</span>
                </div>
              </div>
              <div className="flex justify-between items-center space-x-2">
                <button
                  onClick={() => minusQty(i)}
                  className="px-2 bg-yellow-400 rounded-md"
                >
                  -
                </button>
                <span className="">x{value.qty}</span>
                <button
                  onClick={() => plusQty(i)}
                  className="px-2 bg-yellow-400 rounded-md"
                >
                  +
                </button>
              </div>
              <div className="px-2">
                {customHelper.numberWithCommas(value.price * value.qty)}
              </div>
              <div
                onClick={() => deleteCart(i)}
                className=" hover:scale-110 ease-in-out duration-200 cursor-pointer"
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="w-8 text-red-600"
                  viewBox="0 0 20 20"
                  fill="currentColor"
                >
                  <path
                    fillRule="evenodd"
                    d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z"
                    clipRule="evenodd"
                  />
                </svg>
              </div>
            </div>
          );
        })
      ) : (
        <p className="mt-5">-- No data --</p>
      )}
    </>
  );
}
const mapStateToProps = (state) => {
  return {
    //
  };
};
export default connect(mapStateToProps, {
  deleteCart,
  plusQty,
  minusQty,
})(Cardcart);

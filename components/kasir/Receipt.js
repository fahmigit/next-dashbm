import React, {useState} from "react";
import { useDispatch, useSelector } from "react-redux";
import { customHelper } from "../../helpers";

export const Receipt = React.forwardRef((props, ref) => {
    
  const { numberCart,Carts, tax } = useSelector((state) => state.KasirReducer);

  let subtotal = 0;
  let totalQty = 0;
  let taxValue = 0;
  Carts.map((value) => {
    subtotal += value.price * value.qty;
    totalQty += value.qty;
  });
  if (tax == true) {
    taxValue = (subtotal * 10) / 100;
  } else {
    taxValue = 0;
  }
  let currentdate = new Date();
  let date =
    currentdate.getDate() +
    "/" +
    (currentdate.getMonth() + 1) +
    "/" +
    currentdate.getFullYear();
  let time =
    currentdate.getHours() +
    ":" +
    currentdate.getMinutes() +
    ":" +
    currentdate.getSeconds();
  return (
    <>
      <div ref={ref} id="pdf-view" className="w-4/12 mx-auto px-5 mt-5">
        <div className="flex flex-col text-center">
          <h1 className="font-bold text-xl">Kasir App</h1>
          <span>Jl. Menati 1 No 25</span>
          <span>Karawang</span>
        </div>
        <div>
          <table className="my-2">
            <tr>
              <td>Tanggal</td>
              <td className="px-4">:</td>
              <td>{date}</td>
            </tr>
            <tr>
              <td>Jam</td>
              <td className="px-4">:</td>
              <td>{time}</td>
            </tr>
            <tr>
              <td>Customer</td>
              <td className="px-4">:</td>
              <td>Umum</td>
            </tr>
            <tr>
              <td>Karyawan</td>
              <td className="px-4">:</td>
              <td>Agus</td>
            </tr>
          </table>
          <hr />
          {Carts != "" ? (
            Carts.map((value, i) => {
              return (
                <div key={i} className="pt-2">
                  <h5>{value.name}</h5>
                  <div className="flex justify-between">
                    <div className="">
                      <span>
                        Rp. {customHelper.numberWithCommas(value.price)}
                      </span>
                      <span className="px-3">x {value.qty}</span>
                    </div>
                    <div>
                      Rp{" "}
                      {customHelper.numberWithCommas(value.price * value.qty)}
                    </div>
                  </div>
                </div>
              );
            })
          ) : (
            <p>-- No data --</p>
          )}

          <hr />
          <div>
            <table className="my-2">
              <tr>
                <td>Total item</td>
                <td className="px-4">:</td>
                <td>{numberCart}</td>
              </tr>
              <tr>
                <td>Total Qty</td>
                <td className="px-4">:</td>
                <td>{totalQty}</td>
              </tr>
            </table>
          </div>
          <hr />

          <table className="my-2">
            <tr>
              <td>Sub Total</td>
              <td className="px-4">:</td>
              <td>{customHelper.numberWithCommas(subtotal)}</td>
            </tr>
            <tr>
              <td>Tax</td>
              <td className="px-4">:</td>
              <td>{customHelper.numberWithCommas(taxValue)}</td>
            </tr>
          </table>
          <hr />
          <table className="my-2">
            <tr>
              <td>Total Bayar</td>
              <td className="px-4">:</td>
              <td>Rp. {customHelper.numberWithCommas(subtotal + taxValue)}</td>
            </tr>
          </table>
          <hr />
          <div className="flex flex-col text-center">
            <h1 className="font-bold text-xl">Terimakasih</h1>
            <span>Selamat Berbelanja Kembali</span>
          </div>
        </div>
      </div>
    </>
  );
});

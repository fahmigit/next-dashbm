import React, { useState, useEffect } from "react";
import { useDispatch, useSelector, connect } from "react-redux";
import {
  getListMenu,
  addToCart,
  setMenu,
  setCategoryDropdownMenu,
} from "../../actions/kasirAction";

function Centermenu({ setMenu, setCategoryDropdownMenu }) {
  const { getListMenuResult, menu, categoryDropdownMenu } = useSelector(
    (state) => state.KasirReducer
  );
  // init dispatch
  const dispatch = useDispatch();
  // use effect
  useEffect(() => {
    // call get list cart
    console.log("1. use effect comp did mount");
    dispatch(getListMenu());
  }, [dispatch]);

  // set search query to empty string
  const [query, setQuery] = useState("");

  // init category
  const category = [
    {
      name: "All",
      value: "",
      icon: `<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
              <path stroke-linecap="round" stroke-linejoin="round" d="M11 3.055A9.001 9.001 0 1020.945 13H11V3.055z" />
              <path stroke-linecap="round" stroke-linejoin="round" d="M20.488 9H15V3.512A9.025 9.025 0 0120.488 9z" />
            </svg>`,
    },
    {
      name: "Hot",
      value: "hot",
      icon: `<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
              <path stroke-linecap="round" stroke-linejoin="round" d="M11 3.055A9.001 9.001 0 1020.945 13H11V3.055z" />
              <path stroke-linecap="round" stroke-linejoin="round" d="M20.488 9H15V3.512A9.025 9.025 0 0120.488 9z" />
            </svg>`,
    },
    {
      name: "Makanan",
      value: "makanan",
      icon: `<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
              <path stroke-linecap="round" stroke-linejoin="round" d="M11 3.055A9.001 9.001 0 1020.945 13H11V3.055z" />
              <path stroke-linecap="round" stroke-linejoin="round" d="M20.488 9H15V3.512A9.025 9.025 0 0120.488 9z" />
            </svg>`,
    },
    {
      name: "Minuman",
      value: "minuman",
      icon: `<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
              <path stroke-linecap="round" stroke-linejoin="round" d="M11 3.055A9.001 9.001 0 1020.945 13H11V3.055z" />
              <path stroke-linecap="round" stroke-linejoin="round" d="M20.488 9H15V3.512A9.025 9.025 0 0120.488 9z" />
            </svg>`,
    },
    {
      name: "Kerupuk",
      value: "kerupuk",
      icon: `<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
              <path stroke-linecap="round" stroke-linejoin="round" d="M11 3.055A9.001 9.001 0 1020.945 13H11V3.055z" />
              <path stroke-linecap="round" stroke-linejoin="round" d="M20.488 9H15V3.512A9.025 9.025 0 0120.488 9z" />
            </svg>`,
    },
  ];

  const handlePilihProduk = (payload) => {
    dispatch(addToCart(payload));
  };

  return (
    <>
      <div className="w-full lg:w-8/12 bg-gray-100 p-4 lg:p-10">
        <div>
          <span
            onClick={() => setMenu(!menu)}
            className="flex w-10 p-2 hover:bg-slate-200 rounded-md cursor-pointer"
          >
            {menu == false ? (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                class="h-6 w-6"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
                stroke-width="2"
              >
                <path
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  d="M13 5l7 7-7 7M5 5l7 7-7 7"
                />
              </svg>
            ) : (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                class="h-6 w-6"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
                stroke-width="2"
              >
                <path
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  d="M11 19l-7-7 7-7m8 14l-7-7 7-7"
                />
              </svg>
            )}
          </span>
        </div>
        <h1 className="font-bold text-2xl py-3 flex">
          Category
          <span
            onClick={() => setCategoryDropdownMenu(!categoryDropdownMenu)}
            className="p-2 mx-4 cursor-pointer hover:bg-slate-200 rounded-md"
          >
            {" "}
            {categoryDropdownMenu ? (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                class="h-6 w-6"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
                stroke-width="2"
              >
                <path
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  d="M19 9l-7 7-7-7"
                />
              </svg>
            ) : (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                class="h-6 w-6"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
                stroke-width="2"
              >
                <path
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  d="M5 15l7-7 7 7"
                />
              </svg>
            )}
          </span>{" "}
        </h1>
        <ul
          className={`flex space-x-1 overflow-x-auto  ${
            categoryDropdownMenu == true ? "hidden" : ""
          }`}
        >
          {category.map((value, i) => {
            return (
              <li>
                <div
                  key={i}
                  onClick={() => setQuery(value.value)}
                  className="border-2 rounded-2xl bg-white px-2 py-4 cursor-pointer active:bg-yellow-200 ease-in-out duration-100 hover:scale-110 w-24"
                >
                  <div className=" flex flex-col justify-center items-center">
                    <div className="border-2 rounded-2xl">
                      <div
                        dangerouslySetInnerHTML={{
                          __html: value.icon,
                        }}
                      />
                    </div>
                    <div>
                      <span className="">{value.name}</span>
                    </div>
                  </div>
                </div>
              </li>
            );
          })}
        </ul>

        <div>
          <div className="flex flex-col lg:flex-row justify-between py-5 lg:py-3">
            <h1 className="font-bold text-2xl">Order Menu</h1>
            <div className="pt-2 lg:pt-0">
              <input
                type="text"
                className="px-4 py-2 rounded-lg bg-white"
                placeholder="Search.."
                onChange={(e) => setQuery(e.target.value)}
              />
            </div>
          </div>
          <div className="grid grid-cols-2 lg:grid-cols-5 gap-4 py-0">
            {getListMenuResult ? (
              getListMenuResult
                .filter((value) => {
                  if (query === "") {
                    return value;
                  } else if (
                    value.name.toLowerCase().includes(query.toLowerCase())
                  ) {
                    return value;
                  } else if (
                    value.barcode
                      .toString()
                      .toLowerCase()
                      .includes(query.toString().toLowerCase())
                  ) {
                    return value;
                  } else if (
                    value.kategori.toLowerCase().includes(query.toLowerCase())
                  ) {
                    return value;
                  }
                })
                .map((value, i) => {
                  return (
                    <div
                      onClick={() => handlePilihProduk(value)}
                      key={i}
                      className="border 2 rounded-2xl p-5 bg-white flex flex-col justify-center items-center cursor-pointer hover:scale-105 ease-in-out duration-200"
                    >
                      <div>
                        <img src={value.image} className="w-32 rounded-2xl" />
                      </div>
                      <span className="pt-2 font-bold text-gray-600">
                        {value.name}
                      </span>
                    </div>
                  );
                })
            ) : (
              <p>No data gais</p>
            )}
          </div>
        </div>
      </div>
    </>
  );
}

const mapStateToProps = (state) => {
  return {
    //
  };
};
export default connect(mapStateToProps, {
  setMenu,
  setCategoryDropdownMenu,
})(Centermenu);

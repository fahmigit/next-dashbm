import React, { useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { useDispatch, useSelector, connect } from "react-redux";

function Leftmenu() {
  // get data store
  const { menu } = useSelector((state) => state.KasirReducer);

  const router = useRouter();
  let pathname = router.pathname;
  const [active, setActive] = useState("");
  const sidebar_menu = [
    {
      name: "Home",
      icon: `<svg xmlns="http://www.w3.org/2000/svg"
              class="h-6 w-6"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              stroke-width="2">
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6"
              />
            </svg>`,
      link: "/home",
    },
    {
      name: "Kasir",
      icon: `<svg
              xmlns="http://www.w3.org/2000/svg"
              class="h-6 w-6"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              stroke-width="2"
            >
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                d="M21 15.546c-.523 0-1.046.151-1.5.454a2.704 2.704 0 01-3 0 2.704 2.704 0 00-3 0 2.704 2.704 0 01-3 0 2.704 2.704 0 00-3 0 2.704 2.704 0 01-3 0 2.701 2.701 0 00-1.5-.454M9 6v2m3-2v2m3-2v2M9 3h.01M12 3h.01M15 3h.01M21 21v-7a2 2 0 00-2-2H5a2 2 0 00-2 2v7h18zm-3-9v-2a2 2 0 00-2-2H8a2 2 0 00-2 2v2h12z"
              />
            </svg>`,
      link: "/kasir",
    },
    {
      name: "History",
      icon: `<svg
              xmlns="http://www.w3.org/2000/svg"
              class="h-6 w-6"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              stroke-width="2"
            >
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z"
              />
            </svg>`,
      link: "/history",
    },
    {
      name: "Promo",
      icon: `<svg
              xmlns="http://www.w3.org/2000/svg"
              class="h-6 w-6"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              stroke-width="2"
            >
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6"
              />
            </svg>`,
      link: "/promo",
    },
    {
      name: "Setting",
      icon: `<svg
              xmlns="http://www.w3.org/2000/svg"
              class="h-6 w-6"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              stroke-width="2"
            >
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z"
              />
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
              />
            </svg>`,
      link: "/setting",
    },
  ];
  function handleActive() {
    setActive(true);
  }
  function tooltip(params) {
    
  }
  return (
    <>
      <div
        className={`w-full lg:w-32 lg:py-10 flex lg:flex-col items-start lg:items-center ${menu == true ? 'hidden' : ''} `}
      >
        <Link href={"/"}>
          <a>
            <h1 className="hidden lg:flex font-bold text-2xl">Logo</h1>
          </a>
        </Link>
        <div className="py-5 lg:py-10 overflow-x-auto">
          <ul className="flex lg:flex-col flex-row px-4 space-x-2 lg:space-x-0 lg:space-y-4">
            {sidebar_menu.map((value, i) => {
              return (
                <li
                  onClick={handleActive}
                  key={i}
                  onMouseOver={() => tooltip()}
                >
                  <Link href={value.link}>
                    <a
                      className={`${
                        pathname == value.link ? "bg-yellow-200" : "bg-gray-100"
                      } border rounded-2xl z-10 flex flex-col items-center p-4 hover:bg-yellow-200 hover:scale-105 cursor-pointer hover:text-gray-800 ease-in-out duration-300`}
                    >
                      <div>
                        <div
                          dangerouslySetInnerHTML={{
                            __html: value.icon,
                          }}
                        />
                      </div>
                      <span>{value.name}</span>
                    </a>
                  </Link>
                </li>
              );
            })}
          </ul>
        </div>
      </div>
    </>
  );
}

export default Leftmenu;

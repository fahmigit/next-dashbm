import React, { useState, useEffect } from "react";

function Historymenu() {
  return (
    <>
      <div className="w-full bg-gray-100 p-4 lg:p-10">
        <h1 className="font-bold text-2xl py-3">History</h1>

        <div>
          <div className="flex flex-col lg:flex-row justify-between py-5 lg:py-3">
            <div className="">
              <table className="min-w-full">
                <thead>
                  <tr className="border-b">
                    <th className="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                      No
                    </th>
                    <th className="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                      Invoice
                    </th>
                    <th className="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                      Produk
                    </th>
                    <th className="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                      Total
                    </th>
                    <th className="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                      Aksi
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr className="border-b">
                    <td>1</td>
                    <td>INV12345</td>
                    <td>Bakso X 2</td>
                    <td>Rp. 30.000</td>
                    <td>
                      <button>Print</button>
                    </td>
                  </tr>
                </tbody>
              </table>
              <div className="flex space-x-2 mt-4">
                <button className="bg-slate-200 px-2 rounded-md">
                  Previous
                </button>
                <span className="bg-slate-200 px-2 rounded-md">1</span>
                <span className="bg-slate-200 px-2 rounded-md">2</span>
                <span className="bg-slate-200 px-2 rounded-md">3</span>
                <button className="bg-slate-200 px-2 rounded-md">Next</button>
              </div>
            </div>
          </div>
        </div>
        <div class="flex flex-col">
          <div class="overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div class="py-2 inline-block min-w-full sm:px-6 lg:px-8">
              <div class="overflow-hidden">
                <table class="min-w-full">
                  <thead class="bg-white border-b">
                    <tr>
                      <th
                        scope="col"
                        class="text-sm font-medium text-gray-900 px-6 py-4 text-left"
                      >
                        #
                      </th>
                      <th
                        scope="col"
                        class="text-sm font-medium text-gray-900 px-6 py-4 text-left"
                      >
                        First
                      </th>
                      <th
                        scope="col"
                        class="text-sm font-medium text-gray-900 px-6 py-4 text-left"
                      >
                        Last
                      </th>
                      <th
                        scope="col"
                        class="text-sm font-medium text-gray-900 px-6 py-4 text-left"
                      >
                        Handle
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr class="bg-white border-b transition duration-300 ease-in-out hover:bg-gray-100">
                      <td class="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                        1
                      </td>
                      <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                        Mark
                      </td>
                      <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                        Otto
                      </td>
                      <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                        @mdo
                      </td>
                    </tr>
                    <tr class="bg-white border-b transition duration-300 ease-in-out hover:bg-gray-100">
                      <td class="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                        2
                      </td>
                      <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                        Jacob
                      </td>
                      <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                        Thornton
                      </td>
                      <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                        @fat
                      </td>
                    </tr>
                    <tr class="bg-white border-b transition duration-300 ease-in-out hover:bg-gray-100">
                      <td class="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                        3
                      </td>
                      <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                        Larry
                      </td>
                      <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                        Wild
                      </td>
                      <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                        @twitter
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Historymenu;

import React, { useRef, useState } from "react";
import { useSelector } from "react-redux";
import { connect } from "react-redux";
import ReactToPrint from "react-to-print";
import { setTax, resetCart } from "../../actions/kasirAction";
import { customHelper } from "../../helpers";

function Rightmenu({ setTax, resetCart }) {
  const { numberCart, Carts, tax } = useSelector((state) => state.KasirReducer);

  // init for reactToprint
  const componentRef = useRef();

  let subtotal = 0;
  let taxValue = 0;
  Carts.map((value) => {
    subtotal += value.price * value.qty;
  });
  if (tax == true) {
    taxValue = (subtotal * 10) / 100;
  } else {
    taxValue = 0;
  }

  function handlePay(params) {
    alert("wkwk");
  }
  return (
    <>
      <div className="w-full lg:w-3/12">
        <div className="relative px-4 py-10">
          <h1 className="font-bold text-2xl">Cart ({numberCart})</h1>
          {/* Call cart component */}
          <div className="max-h-80 overflow-y-auto">

          </div>
          {/*  */}
          <div className="flex justify-between mt-10 px-4">
            <span>Subtotal</span>
            <span>Rp. {customHelper.numberWithCommas(subtotal)}</span>
          </div>
          <div className="flex justify-between px-4">
            <span>
              Tax 10% <input onChange={(e) => setTax(!tax)} type="checkbox" />{" "}
            </span>
            <span>Rp. {customHelper.numberWithCommas(taxValue)}</span>
          </div>
          <div className="border-b-4 border-dotted px-4 py-2"></div>
          <div className="flex justify-between px-4 py-3">
            <span>Total</span>
            <span>
              Rp. {customHelper.numberWithCommas(subtotal + taxValue)}
            </span>
          </div>

          <div>
            <h1 className="font-bold text-lg">Payment method</h1>
            <div className="grid grid-cols-3 gap-2 py-2">
              <div className="shadow-md rounded-lg p-4 hover:scale-105 ease-in-out duration-200 cursor-pointer hover:bg-gray-200">
                <span>CASH</span>
              </div>
              <div className="shadow-md rounded-lg p-4 hover:scale-105 ease-in-out duration-200 cursor-pointer hover:bg-gray-200">
                <span>TRANSFER</span>
              </div>
              <div className="shadow-md rounded-lg p-4 hover:scale-105 ease-in-out duration-200 cursor-pointer hover:bg-gray-200">
                <span>QRIS</span>
              </div>
            </div>

          </div>
        </div>
      </div>
    </>
  );
}

const mapStateToProps = (state) => {
  return {
    //
  };
};
export default connect(mapStateToProps, {
  setTax,
  resetCart,
})(Rightmenu);


import { useRouter } from "next/router";
import Leftmenu from "./kasir/Leftmenu";
import Rightmenu from "./kasir/Rightmenu";

export default function Layout({ children }) {
    
  // Init Userouter
  const router = useRouter();

  return (
    <>
      {/* JIKA di path / maka munculkan children aja ga pakai layout menu kiri & kanan */}
      {router.pathname == "/" ||
      router.pathname == "/jsontes" ||
      router.pathname == "/jsontes/create" ||
      router.pathname == "/jsontes/create-react-hook"
       ? (
        children
      ) : (
        <div className="flex flex-col sm:flex-row sm:h-screen">
          {/* RENDER LEFT MENU */}
          <Leftmenu />

          {/* RENDER PAGE PROPS */}
          {children}

          {/* LAYOUT PER PAGE */}
          {router.pathname == "/kasir" ? <Rightmenu /> : ""}
        </div>
      )}
    </>
  );
}

import React from "react";
import Link from "next/link";
import Router from "next/router";

// config api
import { fetchWrapper } from "../helpers";
import getConfig from "next/config";
const { publicRuntimeConfig } = getConfig();
const baseUrl = `${publicRuntimeConfig.apiUrl}`;

function Header() {
  function handleSignout() {
    fetchWrapper.post(`${baseUrl}/sign-out`).then(() => {        
      Router.push("/login");
    });
  }
  return (
    <div className="bg-gray-100 px-5 py-5">
      <div className="flex justify-between">
        <Link href="/">
          <a className="font-bold">Dashboard</a>
        </Link>
        <div>
          <ul className="flex space-x-4">
            <li>
              <Link href="/categorymenus">
                <a className="px-5 py-2 bg-gray-300 rounded-md font-bold">
                  Category Menu
                </a>
              </Link>
            </li>
            <li>
              <Link href="/wilayahs">
                <a className="px-5 py-2 bg-gray-300 rounded-md font-bold">
                  Wilayah
                </a>
              </Link>
            </li>
          </ul>
        </div>
        <button
          onClick={handleSignout}
          className="px-5 py-2 bg-red-300 rounded-md"
        >
          Sign out
        </button>
      </div>
    </div>
  );
}

export default Header;

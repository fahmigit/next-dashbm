import {React, useState, useEffect} from 'react'
import axios from 'axios'
import Link from 'next/link'

function index() {
    const [data, setData] = useState([]);
    const baseURL = "https://jsonplaceholder.typicode.com/posts/";
    useEffect(async () => {
      await axios.get(baseURL).then((res) => {
          console.log(res);
          setData(res.data)
      })
    }, [])
    
    // Style CSS
    const box = {
      border: "solid",
      padding: "1rem",
      marginBottom: "1rem"
    };
    const hsatu = {
      fontSize: "30px",
      fontWeight: "bold",
    };
    const container = {
      margin: "2rem",
    };
  return (
    <div style={container}>
        <h1 style={hsatu}>List Data Post from JsonPlaceholder</h1>
        <Link href="/jsontes/create">
            <a className='px-4 py-2 bg-green-400 rounded-md'>Create Post</a>
        </Link>
        {data.map((value, index) => {
            return (
              <div
                style={box}
                key={index}
              >
                <h1>{value.title}</h1>
                <h3>{value.id}</h3>
                <p>{value.body}</p>
              </div>
            );
        })}
        
    </div>
  )
}

export default index
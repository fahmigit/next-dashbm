import axios from "axios";
import Link from "next/link";
import { React, useState, useEffect } from "react";

function create() {
  const [title, setTitle] = useState("");
  const [body, setBody] = useState("");
  const [req, setReq] = useState(0);
  const [result, setresult] = useState({});
  const [buttonText, setbuttonText] = useState("Submit");
  const baseURL = "https://jsonplaceholder.typicode.com/posts";
  const input = {
    padding: "3px 5px",
    borderRadius: "5px",
    border: "solid",
  };
  const submitForm = async (e) => { 
    e.preventDefault();
    setbuttonText("Loading...");

    if (title != "" && body != "") {
      // CONTOH FETCH TANPA VARIABEL
      //   await axios.post(baseURL, { title: title, body: body }).then((res) => {
      //     console.log(res);
      //     setresult(res.data);
      //     setReq(0);
      //     setbuttonText("Submit");
      //   });

      //   CONTOH FETCH PAKAI VARIABEL
      const response = await axios.post(baseURL, { title: title, body: body });
      console.log(response);
      if (response.status == 201) {
        setresult(response.data);
        setReq(0);
        setTitle('');
        setBody("");
        setbuttonText("Submit");
      } else {
          alert(response.status)
      }
    } else {
      setReq(1);
      setbuttonText("Submit");
    }
  };
  return (
    <div className="m-5">
        <Link href="/jsontes">
            <a className="px-4 py-2 bg-gray-300 rounded-md">Back</a>
        </Link>
      <h1 className="text-lg my-3">Create post</h1>
      <br />
      <form type="post" onSubmit={submitForm}>
        <input
          onChange={(e) => setTitle(e.target.value)}
          style={input}
          type="text"
          value={title}
          placeholder="input title"
        />
        <br />
        <input
          onChange={(e) => setBody(e.target.value)}
          style={input}
          type="text"
          value={body}
          placeholder="input body"
        />
        <br />
        <p className={`text-red-400 ${req == 1 ? "" : "hidden"} `}>
          Oops.. form input wjib diisi semua!
        </p>
        <br />
        <button className="px-2 bg-blue-400 rounded-md text-white">
          {buttonText}
        </button>
        <hr />
        <div>
          Result
          <br />
          result ID : {result.id} <br />
          result Title : {result.title} <br />
          result Body : {result.body}
        </div>
      </form>
    </div>
  );
}

export default create;

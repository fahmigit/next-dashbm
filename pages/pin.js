import {React, useEffect} from 'react'

function pin() {
    useEffect(() => {
      
    const inputs = document.querySelectorAll("input");
    const output = document.getElementById("output");
    console.log(inputs);
    inputs.forEach((input, key) => {
      input.addEventListener("keyup", function () {
        if (input.value) {
            console.log(key);
          if (key === 5) {
            // Last one tadaa
            const userCode = [...inputs].map((input) => input.value).join("");
            output.classList.remove("hidden");
            output.innerText = userCode;
          } else {
            inputs[key + 1].focus();
          }
        }
      });
    });

    const handleDelete = (event) => {
      inputs.forEach((input, key) => {
        input.addEventListener("keyup", function () {
          if (input.value) {
            console.log(key);
            if (key === 5) {
              // Last one tadaa
              if (event.key === "Backspace") {
                // 👇️ your logic here
                console.log("Backspace key pressed ✅");
              }
            } else {
              inputs[key - 1].focus();
            }
          }
        });
      });
    }
    
    }, [])
    
  return (
    <>
      <hr />
      <div className="my-4 w-6/12 mx-auto">
        <h1>Input Your PIN</h1>
        <form className="flex">
          <input
            className="p-2 border-2 rounded-lg w-12 text-center mx-2"
            type="password"
            maxLength="1"
            onKeyDown={()=>handleDelete()}
          />
          <input
            className="p-2 border-2 rounded-lg w-12 text-center mx-2"
            type="password"
            maxLength="1"
          />
          <input
            className="p-2 border-2 rounded-lg w-12 text-center mx-2"
            type="password"
            maxLength="1"
          />
          <input
            className="p-2 border-2 rounded-lg w-12 text-center mx-2"
            type="password"
            maxLength="1"
          />
          <input
            className="p-2 border-2 rounded-lg w-12 text-center mx-2"
            type="password"
            maxLength="1"
          />
          <input
            className="p-2 border-2 rounded-lg w-12 text-center mx-2"
            type="password"
            maxLength="1"
          />
        </form>
        <div className="my-3 hidden" id="output"></div>
      </div>
    </>
  );
}

export default pin
module.exports = {
  reactStrictMode: true,

  publicRuntimeConfig: {
    apiUrl:
      process.env.NODE_ENV === "development"
        ? process.env.URL_API_DEV // development api
        : process.env.URL_API_PROD, // production api
  },
};
